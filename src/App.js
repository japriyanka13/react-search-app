// import logo from './logo.svg';
import './App.css';
import Search from './components/Search';
import initialDetails from './data/initialDetails';

function App() {
  return (
    <div className="App">
      <Search details={initialDetails}/>
    </div>
  );
}

export default App;
