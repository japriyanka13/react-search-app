const initialDetails = [
    {
        id: 1,
        imgPath: "/assets/img/1.png",
        name: "Jiya Deeman",
        email: "janedoe@gmail.com",
        address: "New Delhi, India",
    },
    {
        id: 2,
        imgPath: "/assets/img/2.png",
        name: "Mary Desouza",
        email: "agra@rosie.com",
        address: "Tbilisi, India",
    },
    {
        id: 3,
        imgPath: "/assets/img/3.png",
        name: "Sherlock Desouza",
        email: "irene@johnlock.com",
        address: "Baker Street, India",
    },
    {
        id: 4,
        imgPath: "/assets/img/4.png",
        name: "John Holmes",
        email: "mary@johnlock.com",
        address: "Baker Street, India",
    }
]

export  default initialDetails;